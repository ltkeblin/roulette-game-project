#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 13:33:34 2023

@author: liamkeblin
"""



import tkinter as tk
import random



def print_roulette_rules():
    print("Welcome to Roulette!")
    print("Here are the basic rules:")
    print("\n1. Objective:")
    print("   - The objective of roulette is to correctly guess the outcome of a spin.")
    print("\n2. Betting Options:")
    print("   - Players can place bets on various options, including:")
    print("     - Betting on a specific number (1 to 36)")
    print("     - Betting on red or black")
    print("     - Betting on even or odd numbers")
    print("     - Betting on groups of numbers (e.g., 1-12, 13-24)")
    print("     - and more...")
    print("\n3. Placing Bets:")
    print("   - Players have a limited amount they can wager on each spin.")
    print("   - Bets must be placed before the wheel is spun.")
    print("\n4. Winning and Losing:")
    print("   - If a player's bet matches the outcome of the spin, they win and receive payouts based on the type of bet.")
    print("   - If a player's bet does not match the outcome, they lose their wager.")
    print("\n5. Payouts:")
    print("   - Different bets have different payout ratios. For example:")
    print("     - Betting on a specific number pays 35 times the wager.")
    print("     - Betting on red or black pays 1:1.")
    print("     - Betting on even or odd pays 1:1.")
    print("     - Betting on groups of numbers pays various ratios depending on the size of the group.")
    print("\n6. Ready to Play?")
    print("   - Feel free to place your bets and spin the wheel!")
    
    
    
    
# Graphics for roulette board
# Initialize main window
root = tk.Tk()
root.title("Roulette Board")

# Define colors (0 for green, 1 for red, 2 for black)
colors = {0: "green", 32: "red", 15: "red", 19: "red", 4: "red", 21: "red", 2: "red", 25: "red", 17: "red", 34: "red",
          6: "red", 27: "red", 13: "red", 36: "red", 11: "red", 30: "red", 8: "red", 23: "red", 10: "red", 5: "black",
          24: "black", 16: "black", 33: "black", 1: "black", 20: "black", 14: "black", 31: "black", 9: "black",
          22: "black", 18: "black", 29: "black", 7: "black", 28: "black", 12: "black", 35: "black", 3: "black", 26: "black"}

# Function to draw the roulette board
def draw_roulette_board():
    canvas = tk.Canvas(root, width=400, height=400)
    canvas.pack()

    for i in range(12):
        for j in range(3):
            number = i*3 + j + 1
            color = colors.get(number, "white")
            canvas.create_rectangle(j*100, i*100, (j+1)*100, (i+1)*100, fill=color)
            canvas.create_text(j*100+50, i*100+50, text=str(number), font=("Arial", 20))

    canvas.create_oval(50, 50, 350, 350, width=2)

# Draw the roulette board
draw_roulette_board()

# Start the main event loop
root.mainloop()


# Call the function to print out the rules
print_roulette_rules()

# Initialize player balances
player_balances = [1000, 1000, 1000, 1000, 1000]  # Starting balance for each player

# Function to handle distributing winnings and updating balances
def distribute_winnings(result):
    global player_balances

    for i in range(5):
        if bets[i].get() > 0:
            if bets_on.get() == result or (bets_on_color.get() and colors[result] == bets[i].get()):
                player_balances[i] += bets[i].get() * 35  # Player wins if they bet on the correct number




# Game Code

# Initialize player balances
player_balances = [1000, 1000, 1000, 1000, 1000]  # Starting balance for each player

# Function to get player wagers and pool the money
def get_wagers():
    wagers = []
    total_wagered = 0
    for i in range(5):
        wager = int(input(f"Player {i+1}, enter your wager (max $1000): "))
        while wager > 1000 or wager < 0:
            wager = int(input(f"Invalid input. Player {i+1}, enter your wager (max $1000): "))
        wagers.append(wager)
        total_wagered += wager
    return wagers, total_wagered

# Function to determine the winner and update balances
def determine_winner(wagers, total_wagered):
    global player_balances

    result = random.randint(0, 36)
    print(f"Result: {result}")

    winning_player = None
    for i in range(5):
        if wagers[i] > 0:
            if bets[i] == result:
                winning_player = i
                break

    if winning_player is not None:
        winnings = total_wagered - wagers[winning_player]
        player_balances[winning_player] += winnings
        print(f"Player {winning_player + 1} wins ${winnings}!")
    
    for i in range(5):
        if i != winning_player:
            player_balances[i] -= wagers[i]

# Get wagers and pool money
bets, total_bet = get_wagers()

# Spin the wheel, determine the winner, and update balances
determine_winner(bets, total_bet)

# Display updated balances
for i, balance in enumerate(player_balances):
    print(f"Player {i+1} Balance: ${balance}")







# Initialize player balances
player_balances = [1000, 1000, 1000, 1000, 1000]  # Starting balance for each player

# Function to handle spinning the wheel
def spin():
    global player_balances

    result = random.randint(0, 36)
    result_label.config(text=f"Result: {result}")

    # Calculate winnings/losses for each player
    for i in range(5):
        if bets[i].get() > 0:
            if bets_on.get() == result or (bets_on_color.get() and colors[result] == bets[i].get()):
                player_balances[i] += bets[i].get() * 35  # Player wins if they bet on the correct number
            else:
                player_balances[i] -= bets[i].get()  # Player loses

        # Update balance labels
        balance_labels[i].config(text=f"Player {i+1} Balance: ${player_balances[i]}")

# Create the main window
root = tk.Tk()
root.title("Roulette Game")

# Create a label for result display
result_label = tk.Label(root, text="Result: ")
result_label.pack()

# Create a label for players to bet on a number
bets_on_label = tk.Label(root, text="Bet on number: ")
bets_on_label.pack()

# Entry for players to choose the number to bet on
bets_on = tk.Entry(root)
bets_on.pack()

# Create a label for players to bet on a color
bets_on_color_label = tk.Label(root, text="Bet on color (1 for red, 2 for black): ")
bets_on_color_label.pack()

# Entry for players to choose the color to bet on
bets_on_color = tk.Entry(root)
bets_on_color.pack()

# Create labels for player balances
balance_labels = []
for i in range(5):
    balance_label = tk.Label(root, text=f"Player {i+1} Balance: ${player_balances[i]}")
    balance_label.pack()
    balance_labels.append(balance_label)

# Entry fields for player bets
bets = []
for i in range(5):
    bet_entry = tk.Entry(root)
    bet_entry.pack()
    bets.append(bet_entry)

# Button to spin the wheel
spin_button = tk.Button(root, text="Spin Wheel", command=spin)
spin_button.pack(pady=10)

# Define colors (0 for green, 1 for red, 2 for black)
colors = {0: 0, 32: 1, 15: 1, 19: 1, 4: 1, 21: 1, 2: 1, 25: 1, 17: 1, 34: 1, 6: 1, 27: 1, 13: 1, 36: 1, 11: 1,
          30: 1, 8: 1, 23: 1, 10: 1, 5: 2, 24: 2, 16: 2, 33: 2, 1: 2, 20: 2, 14: 2, 31: 2, 9: 2, 22: 2, 18: 2, 29: 2,
          7: 2, 28: 2, 12: 2, 35: 2, 3: 2, 26: 2}

# Run the main event loop
root.mainloop()

# ... (rest of your code remains the same)


